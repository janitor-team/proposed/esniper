Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: esniper
Source: https://sourceforge.net/projects/esniper/
Files-Excluded:
    esniper.mdzip
    frontends/php/ebay_logo.gif
Comment:
 This package was debianized by Dima Barsky <dima@debian.org>
 on Fri, 30 Mar 2007 16:25:35 +0100.

Files: *
Copyright: 2002-2007 Scott Nicol <esniper@users.sf.net>
License: BSD-2-Clause

Files: frontends/php/*
Copyright: 2005 Nils Rottgardt <nils@rottgardt.org>
License: BSD-2-Clause

Files: frontends/php/ez_sql.php
Copyright: Justin Vincent <justin@visunet.ie>
License: LGPL-3

Files: debian/*
Copyright:
    2007-2014 Dima Barsky <dima@debian.org>
    2016-2019 Dmitry Smirnov <onlyjob@debian.org>
License: GPL-1+

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
  - Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
  - Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in the
    documentation and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
 OWNER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
 SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED
 TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

License: GPL-1+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 1, or (at your option)
 any later version.
 ․
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU General Public
 License Version 1 can be found in "/usr/share/common-licenses/GPL-1".

License: LGPL-3
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Lesser General Public
 License as published by the Free Software Foundation;
 version 3 of the License.
 ․
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Lesser General Public License for more details.
 ․
 On Debian systems, the complete text of the GNU Lesser General Public
 License Version 3 can be found in "/usr/share/common-licenses/LGPL-3".
